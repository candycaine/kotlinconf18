package io.ktor.samples.filelisting

// ...

fun main(args: Array<String>) {

    val server = EmbeddedServer(Netty, 8080)
    server.install(DefaultHeaders)
    server.install(CallLogging)
    
    server.routing()
        .get("/").addHandler { 
            it.call.respondRedirect("/myfiles")
        }
        .get("/info").addHandler {
            it.call.respondInfo()
        }
        .route("/myfiles").addHandler {
            it.files(root)
            it.listing(root)
        }
    }
    
    server.start(true)
}