package net.sarazan.kotlinconf18

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import net.sarazan.host.HostInterface
import net.sarazan.host.PluginHost
import net.sarazan.host.StandardHostInterface
import net.sarazan.sampleplugin.samplePlugin
import net.sarazan.sampleplugin.samplePlugin2

class MainActivity : AppCompatActivity() {

    private val plugins: PluginHost by lazy {
        PluginHost.Builder(this)
                .addPlugin(samplePlugin)
                .addPlugin(samplePlugin2)
                .build(StandardHostInterface::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        recycler.layoutManager = LinearLayoutManager(this)
        recycler.adapter = plugins.getCardAdapter()
    }

}
