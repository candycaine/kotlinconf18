package net.sarazan.plugin

import android.net.Uri
import android.support.annotation.IdRes
import net.sarazan.plugin.alerts.PluginAlert
import net.sarazan.plugin.alerts.PluginAlertButton
import net.sarazan.plugin.alerts.PluginAlertToasts
import net.sarazan.plugin.alerts.PluginAlerts
import net.sarazan.plugin.handlers.PluginHandlers


fun plugin(id: String, fn: Plugin.Builder.() -> Unit): Plugin {
    return Plugin.Builder(id).apply(fn).build()
}

fun Plugin.Builder.alerts(fn: PluginAlerts.Builder.() -> Unit) {
    setAlerts(PluginAlerts.Builder().apply(fn).build())
}

fun PluginAlerts.Builder.alert(@IdRes id: Int, fn: PluginAlert.Builder.() -> Unit) {
    alert(PluginAlert.Builder(id).apply(fn).build())
}

fun PluginAlert.Builder.button(text: String, action: String? = null): PluginAlertButton {
    return PluginAlertButton(text, action)
}

fun PluginAlerts.Builder.toasts(fn: PluginAlertToasts.Builder.() -> Unit) {
    toasts = PluginAlertToasts.Builder().apply(fn).build()
}

fun Plugin.Builder.handlers(fn: PluginHandlers.Builder.() -> Unit) {
    setHandlers(PluginHandlers.Builder().apply(fn).build())
}